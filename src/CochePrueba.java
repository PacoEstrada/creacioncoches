public class CochePrueba {

    public static void main(String[] args) {

        //creamos el coche 1

        Coche coche1 = new Coche();


        coche1.dueño = "Manoli";
        coche1.marca = "Volswagen";
        coche1.modelo = "Golf";


        //creamos el coche 2

        Coche coche2 = new Coche();

        coche2.modelo = "Cordoba";
        coche2.marca = "Seat";
        coche2.dueño = "Sergio";


        //creamos el coche 3

        Coche coche3 = new Coche();

        coche3.dueño = "Fran";
        coche3.marca = "Skoda";
        coche3.modelo = "Fabia";


        //imprimimos los datos de los tres coches, llamando al metodo creado.

        System.out.println("\n Los datos de los 3 coches son:\n");

                coche1.datosCoche();
        System.out.println("\n");
                coche2.datosCoche();
        System.out.println("\n");
                coche3.datosCoche();






    }
}
